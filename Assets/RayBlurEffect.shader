﻿Shader "Hidden/RayBlurEffect"
{
    
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _CoC ("CoC", 2D) = "CoC" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        
        //pass 0
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            sampler2D _CameraDepthTexture;
            float _Distance;
            float _LensCoeff;  
            float _RcpMaxCoC;
            float _cocMultiplier;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };
 
            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float depth = tex2D(_CameraDepthTexture, i.uv).r;
                depth = LinearEyeDepth(depth);
                half coc = (depth - _Distance)/depth * _LensCoeff;
                coc*=_cocMultiplier;
                coc=2*(saturate(0.5+coc*0.5)-0.5);
                fixed4 col = tex2D(_MainTex, i.uv);
                col.a=-coc;
                return col;

                //return value between -1 and 1 representing circle of confusion
                //plus forward/backward in linear distance space
            }

            ENDCG
        }
        
        //pass 1
        Pass
        {
            CGPROGRAM
            #pragma target 5.0
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "SimplexNoise3D.cginc"
            #define PI 3.1414

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float4 _MainTex_TexelSize;
            float _RcpAspect;
            float _blurAmount;

            StructuredBuffer<float2> _Samples;

            int _SampleCount;
            int _MaxSampleCount;
            int _SampleSetSize;

            StructuredBuffer<float> _Kernel;
            float _KernelSize;
            int _temporalNoise;

            float rand(float2 co){
                return frac(sin(dot(co.xy ,float2(12.9898,78.233))) * 43758.5453);
            }

            fixed4 frag (v2f i) : SV_Target
            {

                fixed4 col0 = tex2D(_MainTex, i.uv);
                float coc0=col0.a*_blurAmount;

                half4 bgAcc = half4(col0.rgb,1)*0.5;
                float randomOffset=_Time*60*_temporalNoise;
                int k=(int)(rand(i.uv+randomOffset)*_SampleSetSize);
    
                for (int si = 0; si < _MaxSampleCount; si++)
                {
                    if (bgAcc.a<_SampleCount) 
                    {
                        
                        float2 disp=_Samples[k]*_blurAmount;
                        float2 offset = float2(disp.x * _RcpAspect, disp.y);
                        offset+=i.uv;
                        fixed4 colN = tex2D(_MainTex, offset);
                        half cocN=colN.a*_blurAmount;

                        half bgCoC = max(coc0,cocN);
                        half dist = length(disp);
                        half weight=step(dist,abs(coc0)); //ignore points to far away

                        //apply kernel to exclude points probabilistically
                        int kernelLookup=(int)(_KernelSize*dist/abs(bgCoC));
                        float kv=_Kernel[kernelLookup];
                        float rn=rand(offset+randomOffset);
                        weight*=step(rn,kv);

                        //accumulate pixel value
                        bgAcc += half4(colN.rgb,1.0)*weight;
                        
                        int ks=1+(int)(rn*(_SampleSetSize*0.25));
                        k=(k+ks)%_SampleSetSize;
                    }
                    
                }
                // Get the weighted average.
                bgAcc.rgb=(bgAcc.a == 0.0)?0:bgAcc.rgb/bgAcc.a;
                return bgAcc;
            }
            ENDCG
        }
    }
}
