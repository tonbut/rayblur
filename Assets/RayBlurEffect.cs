﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class RayBlurEffect : MonoBehaviour
{

    public Shader _shader;

    [Header ("Aperture")]
    [Range(0.1f, 8f), Tooltip("Distance to the point of focus.")]
    public float focusDistance =  10f ;

    /// <summary>
    /// The ratio of the aperture (known as f-stop or f-number). The smaller the value is, the
    /// shallower the depth of field is.
    /// </summary>
    [Range(1f, 22f), Tooltip("Ratio of aperture (known as f-stop or f-number). The smaller the value is, the shallower the depth of field is.")]
    public float aperture = 5.6f ;

    /// <summary>
    /// The distance between the lens and the film. The larger the value is, the shallower the
    /// depth of field is.
    /// </summary>
    [Range(1f, 300f), Tooltip("Distance between the lens and the film. The larger the value is, the shallower the depth of field is.")]
    public float focalLength = 50f ;

    public enum KernelShape
    {
        Smooth, Circle, Outline
    };
    public KernelShape kernelShape = KernelShape.Circle;


    [Header ("Rendering")]
    [Range(0.01f, 0.1f)]
    public float blurRadius= 0.02f;
    [Range(1f, 4f)]
    public float blurRadiusMultiplier = 1f;

    [Min(1)]
    public int sampleSetSize=128;
    [Range(1, 64)]
    public int sampleCount=1;
    [Range(1, 128)]
    public int maxSampleCount=32;

    public bool temporalNoise=false;

    const float k_FilmHeight = 0.024f/20f;
    Material _material;

    void Start(){
        Camera cam = GetComponent<Camera>();
        cam.depthTextureMode = cam.depthTextureMode | DepthTextureMode.Depth;
    }

    ComputeBuffer mSamplesBuffer;
    KernelShape mLastKernelShape;
    ComputeBuffer mKernelWeightBuffer;
    const int KERNEL_WEIGHT_LOOKUP_SIZE=9;
    float[] KERNEL_SMOOTH= new float[] { 1,1,1,0.9f,0.7f,0.5f,0.2f,0.1f,0.05f };
    float[] KERNEL_OUTLINE= new float[] { 0,0,0,0,0,0,0,1,1 };
    float[] KERNEL_CIRCLE= new float[] { 1,1,1,1,1,1,1,1,1 };
    

    void CreateSampleBuffer() {
        if (mSamplesBuffer==null || mSamplesBuffer.count!=sampleSetSize || kernelShape!=mLastKernelShape) {
            ReleaseBuffers();
            mLastKernelShape=kernelShape;
            mSamplesBuffer = new ComputeBuffer(sampleSetSize, sizeof(float) * 2);
            
            Vector2[] sampleData = new Vector2[sampleSetSize];
            sampleData[0]=new Vector2(0,0);
            for (int i=0; i<sampleSetSize; i++) {
                float angle=2.0f*Mathf.PI*Random.value;
                float dist=Mathf.Sqrt(Random.value);
                
                sampleData[i]=new Vector2(Mathf.Sin(angle)*dist,Mathf.Cos(angle)*dist);
            }
            mSamplesBuffer.SetData(sampleData);

            mKernelWeightBuffer= new ComputeBuffer(KERNEL_WEIGHT_LOOKUP_SIZE, sizeof(float) );
            if (kernelShape==KernelShape.Outline) {
                mKernelWeightBuffer.SetData(KERNEL_OUTLINE);
            }
            else if (kernelShape==KernelShape.Circle) {
                mKernelWeightBuffer.SetData(KERNEL_CIRCLE);
            }
            else {
                mKernelWeightBuffer.SetData(KERNEL_SMOOTH);
            }
            
        }
    }

    void ReleaseBuffers() {
        if (mSamplesBuffer!=null) {
            mSamplesBuffer.Dispose();
            mSamplesBuffer=null;
        }
        if (mKernelWeightBuffer!=null) {
            mKernelWeightBuffer.Dispose();
            mKernelWeightBuffer=null;
        }
    }


    float CalculateMaxCoCRadius(int screenHeight)
    {
        // Estimate the allowable maximum radius of CoC from the kernel
        // size (the equation below was empirically derived).
        float radiusInPixels = (float)2 * 4f + 6f;

        // Applying a 5% limit to the CoC radius to keep the size of
        // TileMax/NeighborMax small enough.
        return Mathf.Min(0.05f, radiusInPixels / screenHeight);
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (_material == null)
        {
            _material = new Material(_shader);
            _material.hideFlags = HideFlags.DontSave;
        }
        

        RenderTextureFormat rtf=source.format;
        

        // Step 1
        RenderTexture coc=RenderTexture.GetTemporary(
            source.width,
            source.height,
            0, //depth buffer bits
            rtf, 
            RenderTextureReadWrite.Linear
        );

        float scaledFilmHeight = k_FilmHeight * (source.height / 1080f);
        var f = focalLength / 1000f;
        var s1 = Mathf.Max(focusDistance, f);
        var aspect = (float)source.width / (float)source.height;
        var coeff = f * f / (aperture * (s1 - f) * scaledFilmHeight * 2f);
        var maxCoC = CalculateMaxCoCRadius(source.height);

        
        _material.SetFloat("_Distance",focusDistance);
        _material.SetFloat("_LensCoeff",coeff);
        _material.SetFloat("_RcpMaxCoC",1f/maxCoC);
        _material.SetFloat("_RcpAspect", 1f / aspect);
        _material.SetFloat("_cocMultiplier",blurRadiusMultiplier);
        Graphics.Blit(source, coc, _material, 0);
        

        // Step 2
        CreateSampleBuffer();
        //_material.SetTexture("_CoC",coc);
        _material.SetBuffer("_Samples",mSamplesBuffer);
        _material.SetInt("_SampleSetSize",sampleSetSize);
        _material.SetInt("_SampleCount",sampleCount);
        _material.SetInt("_MaxSampleCount",maxSampleCount);

        _material.SetBuffer("_Kernel",mKernelWeightBuffer);
        _material.SetFloat("_KernelSize",KERNEL_WEIGHT_LOOKUP_SIZE-1);
        
        _material.SetFloat("_blurAmount",blurRadius);
        _material.SetInt("_temporalNoise",temporalNoise?1:0);
        
        Graphics.Blit(coc, destination, _material, 1);

        RenderTexture.ReleaseTemporary(coc);
    }
}
