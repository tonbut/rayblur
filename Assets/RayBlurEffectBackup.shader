Shader "Hidden/RayBlurEffectBackup"
{
    
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _CoC ("CoC", 2D) = "CoC" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        
        //pass 0
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            sampler2D _CameraDepthTexture;
            float _Distance;
            float _LensCoeff;  
            float _RcpMaxCoC;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };
 
            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                float depth = tex2D(_CameraDepthTexture, i.uv).r;
                depth = Linear01Depth(depth)* _ProjectionParams.z;
                //depth=min(depth,100);
                half coc = (depth - _Distance) * _LensCoeff;
                coc=2*(saturate(0.5+coc*0.5)-0.5);
                static half4 maxBlur=1;
                //return clamp(coc*maxBlur,-1,1);
                return half4(-coc,0,0,0);

                //return value between -1 and 1 representing circle of confusion
                //plus forward/backward in linear distance space
            }

            ENDCG
        }
        

        
        //pass 1
        Pass
        {
            CGPROGRAM
            #pragma target 5.0
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "SimplexNoise3D.cginc"
            #define PI 3.1414

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float4 _MainTex_TexelSize;
            sampler2D _CoC;
            float _RcpAspect;
            float _blurAmount;

            StructuredBuffer<float2> _Samples;

            int _SampleCount;
            int _SampleSetSize;
/*
            static const int kSampleCount = 16;
            static const float2 kDiskKernel[kSampleCount] = {
                float2(0,0),
                float2(0.54545456,0),
                float2(0.16855472,0.5187581),
                float2(-0.44128203,0.3206101),
                float2(-0.44128197,-0.3206102),
                float2(0.1685548,-0.5187581),
                float2(1,0),
                float2(0.809017,0.58778524),
                float2(0.30901697,0.95105654),
                float2(-0.30901703,0.9510565),
                float2(-0.80901706,0.5877852),
                float2(-1,0),
                float2(-0.80901694,-0.58778536),
                float2(-0.30901664,-0.9510566),
                float2(0.30901712,-0.9510565),
                float2(0.80901694,-0.5877853),
            };*/

            static float cocScale=1;

            fixed4 frag (v2f i) : SV_Target
            {
                float screenHeight=_ScreenParams.y;

                fixed4 col0 = tex2D(_MainTex, i.uv);
                float coc0=tex2D(_CoC, i.uv).r*_blurAmount;

                //fixed4 c=col0;
                half4 bgAcc = 0.0; // Background: far field bokeh
                half4 fgAcc = 0.0; // Foreground: near field bokeh

                float2 uv=i.uv*1773;

                //for (int si = 0; si < kSampleCount; si++)
                float noise2=(noise(float3(uv.x, uv.y, 0)).x);
                int so=(int)(noise2*_SampleSetSize/2);//(int)(saturate(noise2)*_SampleSetSize);
                so=abs(so);
                

                for (int si = 0; si < _SampleCount; si++)
                {
                    int k=(si+so)%_SampleSetSize;
                    //if (k>_SampleSetSize) k-=_SampleSetSize;
 
                    float2 disp=_Samples[k]*_blurAmount;
                    float dist = length(disp);
                    float2 offset = float2(disp.x * _RcpAspect, disp.y);
                    offset+=i.uv;
                    fixed4 colN = tex2D(_MainTex, offset);
                    //half4 cocN=cocScale*tex2D(_CoC, offset);
                    float cocN=tex2D(_CoC, offset).r*_blurAmount;

                    if (cocN>coc0) { //sample in front of pixel
                        if (abs(cocN)>dist) { //coc spreads back to pixel
                            float weight=1-abs(cocN); //bigger coc means lower weighting
                            bgAcc += half4(colN.rgb,weight);
                        }
                    }
                    else { // sample is behind pixel
                        half bgCoC = max(coc0,cocN);
                        if (abs(bgCoC)>=dist) {
                            bgAcc += half4(colN.rgb,1.0);
                        }
                        
                    }
                    

                    /*
                    //c+=colN*(1-abs(cocN));

                    // BG: Compare CoC of the current sample and the center sample
                    // and select smaller one.
                    half bgCoC = min(max(coc0,cocN), 0.0);

                    // Compare the CoC to the sample distance.
                    // Add a small margin to smooth out.
                    const half margin = _MainTex_TexelSize.y * 2;
                    half bgWeight = saturate((-bgCoC   - dist + margin) / (margin));
                    bgWeight=abs((cocN)/_blurAmount);//0.0+saturate(-coc0/_blurAmount   );
                    half fgWeight = saturate((-cocN - dist + margin) / (margin));

                    // Cut influence from focused areas because they're darkened by CoC
                    // premultiplying. This is only needed for near field.
                    fgWeight *= step(_MainTex_TexelSize.y, -cocN);
                    */
                    //fgWeight=0;
                    
                    
                    /*
                    float c2=cocN/cocScale;
                    float area=c2*c2;
                    float prob=lerp(1,0.5,area);
                    float noise2=(noise(float3(uv.x*10,uv.y*10,si)).x);
                    float s=step(noise2,prob);
                    bgWeight*=s;
                    fgWeight*=s;
                    */

                    

                    // Accumulation
                    //bgAcc += half4(colN.rgb,1.0)* bgWeight;
                    //fgAcc += half4(colN.rgb,1.0)* fgWeight;
                }
                // Get the weighted average.
                bgAcc.rgb /= bgAcc.a + (bgAcc.a == 0.0); // zero-div guard
                fgAcc.rgb /= fgAcc.a + (fgAcc.a == 0.0);
                if (bgAcc.a==0) bgAcc.rgb=col0.rgb;
                if (fgAcc.a==0) fgAcc.rgb=col0.rgb;

                // FG: Normalize the total of the weights.
                //fgAcc.a *= PI / _SampleCount;
                //fgAcc.a*=4;

                // Alpha premultiplying
                half alpha = saturate(fgAcc.a);
                //fgAcc.rgb=0;
                //bgAcc.rgb=0;
                //half3 rgb = lerp(bgAcc.rgb, fgAcc.rgb, alpha);
                half3 rgb = bgAcc.rgb;

                //return half4(rgb, 0);
                //return half4(0.5*(coc0+1));
                float ba=abs(coc0/_blurAmount);
                return half4(rgb, 0);//+half4(0,0,(1-ba)*0.25,0);
            }
            ENDCG
        }
    }
}
