﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour
{
    public float radius=0.5f;
    public float speed=0.1f;
    public float offset=0f;
    public Vector3 rotationOffset=new Vector3();
    private Vector3 initialPosition;

    // Start is called before the first frame update
    void Start()
    {
        initialPosition=transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        float rot=(Time.time+offset)*speed;
        float x=Mathf.Sin(rot);
        float y=Mathf.Cos(rot);
        Vector3 position=initialPosition+new Vector3(x*radius,0,y*radius);
        transform.localPosition=position;
        Quaternion r=Quaternion.identity;
        r.eulerAngles=rotationOffset+new Vector3(0,90+rot/Mathf.PI*180);
        transform.rotation=r;
    }
}
