# Unity Ray Blur

Focus blur is a technique in computer graphics to create the bokeh affect that occurs in photography when real lenses with (inevitably) non-zero sized apertures are used. The lens must be focused at a specific distance and any objects either in-front or behind that focus distance will be progressively blurred. The kind of bokeh that occurs depends upon the many tradeoffs that went into the design of the lens.

Focus blur as implemented in ray-tracers trades off performance for a smoother, less noisy result. The reason for this is that multiple rays are needed for each pixel to simulate the light captured by a non-zero sized (not pinhole) aperture. Usually the area of the aperture is stochastically (randomly) sampled and the more samples made the closer to a perfect capture you get. The results are then averaged. Hence the more samples the smoother the result. The effect of this is that out of focus areas get progressively more grainy the more out of focus they are. 

In high performance rastering renderers, such as those in modern computer games, focus blur is a post-processing effect. After an image frame has been rendered the blur effect is applied to it. This requires a depth map to be captured as part of the rendering. The depth map is an image where each pixel contains the distance from the camera to the surface that provides the colour for that pixel. A depth map is quite cheap to produce as part of the modern rendering pipelines. The blur produced by this approach is very smooth.

I personally like the aesthetic of ray-traced focus blur much more. It has a more "analog" film grain like feel. When I moved to do more work with Unity I was dissapointed by the shiny smooth synthetic blur produced by it's post-processing pipeline. That was the motivation for this project.

For more information see [my blog post](https://blog.durablescope.com/post/UnityRayBlur/).

![Example1](https://gitlab.com/tonbut/rayblur/-/raw/master/exampleImages/rayblurExample2.jpg)

![Example2](https://gitlab.com/tonbut/rayblur/-/raw/master/exampleImages/rayblurExample3.jpg)

Performance is similar to built-in blur using post-processing effects with sensible settings. Extreme settings can get quite slow.

## Parameters
- **Focus Distance** - distance to the point of focus
- **Aperture** - ratio of aperture (known as f-stop or f-number). The smaller the value is, the shallower the depth of field is.
- **Focal Length** - the distance between the lens and the film. The larger the value is, the shallower the depth of field is
- **Kernel Shape** - [Smooth, Circle, Outline] Smooth uses a gaussian blur. Circle simulates a traditional circular camera aperture. Outline creates a more sharp bokeh characteristic of mirror or overcorrected lenses.
- **Blur Radius** - how large the maximum blur can be
- **Blur Radius Multipler** - scales the actual blur to use more of the blur dynamic range set by blur radius
- **Sample Set Size** - the number of random points to sample from. Large numbers will cause totally random looking noise, smaller values will start to look more like a pattern. This parameter doesn't effect performance
- **Sample Count** - the number of random samples tested for each pixel
- **Max Sample Count** - many samples fall out of the range of the circle of confusion, once max sample count samples have fallen inside, and are used to effect the result, no more will be used. Keeping this value small keeps performance up.
- **Temporal Noise** - When true the noise is varied on each frame, this looks more realistic and smoother but can cause a big increase in size and/or reduced quality of rendered video.